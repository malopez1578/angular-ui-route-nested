let app = angular.module('angular', ['ui.router', 'ngAnimate','ngSanitize']);

let format = '.html',
    fn_onEnter = function($state, $stateParams){
        document.querySelector('.o-viewHelper[ui-view]').classList.toggle('is-active');
        document.querySelector('body').classList.toggle('u-overflow-hidden');
    },
    fn_onExit = function($state, $stateParams){
        console.log(`view ${$state.current.name} onExit.`)
        document.querySelector('.o-viewHelper[ui-view]').classList.toggle('is-active');
        document.querySelector('body').classList.toggle('u-overflow-hidden');
    };

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('index', {
            url: '/',
            views: {
                '': {
                    templateUrl: function (stateParams) {
                        return 'template_part/main' + format
                    }
                },
                'header@index': {
                    templateUrl: function (stateParams) {
                        return 'template_part/header' + format
                    }
                },
                'section@index': {
                    templateUrl: function (stateParams) {
                        return 'template_part/section' + format
                    }
                },
                'footer@index': {
                    templateUrl: function (stateParams) {
                        return 'template_part/footer' + format
                    }
                },
            }
        })
        .state('index.activity', {
            url: '',
            params: { modulo: null, act : null },
            views: {
                'activity': {
                    templateUrl: function (stateParams) {
                        return `views/${stateParams.modulo}/${stateParams.act}` + format
                    }
                },
            },
            onEnter : ($state, $stateParams) => fn_onEnter($state, $stateParams),
            onExit : ($state, $stateParams) => fn_onExit($state, $stateParams)
        })

    $urlRouterProvider.otherwise('/index');
}]);