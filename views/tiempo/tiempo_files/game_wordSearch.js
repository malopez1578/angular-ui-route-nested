function fn_gameWordSearch(PuzzleContent_id, arrWords, numberCellsExedent){
    // 	============================================
    var id_PuzzleContent = PuzzleContent_id+'_content'
    var id_tableAnswers = PuzzleContent_id+'_answers'
    var id_tablePuzzle = PuzzleContent_id+'_puzzle'
    var id_tablePuzzleCell = PuzzleContent_id+'_cell_'
    // 	============================================
    // Variables 
    var trace = console.log.bind(console);
    var wordList =  arrWords;
    var alphabet = ["a","b","c","d","e","f","g","h","i",
                         "j","k","l","m","n","o","p","q","r",
                         "s","t","u","v","w","x","y","z"];
    var answeredList = [];
    var rand1 = 0;
    var rand2 = 0;
    var wLength = 0;
    var writable = false;
    var totalWordLength = 0;
    var maxWordLength = 0;
    // var timer = "00:00.00";
    var tick = 0;
    // var timerStarted = false;
    var allAnswered = false;

    function sizeSort(a, b){
        if (a.length > b.length){
            return 1;
        }
        else if (a.length == b.length) {
            return 0;
        }
        else {
            return -1;
        }
    }

    wordList.sort(sizeSort);

    // Pass width + height and it returns a 2D Array
    function create2dArray(width, height){
        var x = new Array(height);
        for (var i = 0; i < height; i++) {
            x[i] = new Array(width);
        }
      return x;
    }

    //Find the length of the longest word
    for (var i = 0; i < wordList.length; i++){
        if (wordList[i].length > maxWordLength){
            maxWordLength = wordList[i].length;
        }
        totalWordLength += wordList[i].length;
    }

    var dimensions = (Math.ceil(Math.sqrt(totalWordLength)) + numberCellsExedent);

    if(dimensions < maxWordLength){
        dimensions = (maxWordLength + 5);
    }

    var wordSearch = create2dArray(dimensions, dimensions);
    

    // Add a Horizontal Word to the array.
    function addH(word, array){
        wLength = word.length;
        rand1 = Math.floor(Math.random()*(wordSearch.length));
        rand2 = Math.floor(Math.random()*((wordSearch.length) - wLength));

        if (rand1 < 0){
            rand1 = 0;
        }
        if (rand2 < 0){
            rand2 = 0;
        }
        if ((rand2+wLength) <= array[rand1].length){
        for(var n = 0; n < wLength; n++){
                if (typeof array[rand1][rand2+n] == 'undefined' || array[rand1][rand2+n] === null || array[rand1][rand2+n] == word.charAt(n)){
                    writable = true;
                }
                else {
                    writable = false;
                    break;
                }
            }
        }

        if (writable && (rand2+wLength) <= array[rand1].length){
            for (var i = 0; i < wLength; i++){
                    array[rand1][rand2+i] = word.charAt(i);
            }
            return true;
        }
        else {
            return false;
        }
    }

    function addV(word, array){
        wLength = word.length;
        rand1 = Math.floor(Math.random()*(wordSearch.length - wLength));
        rand2 = Math.floor(Math.random()*wordSearch.length);

        if (rand1 < 0){
            rand1 = 0;
        }
        if (rand2 < 0){
            rand2 = 0;
        }
        if ((rand1+wLength) <= array.length){
            for(var n = 0; n < wLength; n++){
                if (typeof array[rand1+n][rand2] == 'undefined' || array[rand1+n][rand2] === null || array[rand1+n][rand2] == word.charAt(n)){
                    writable = true;
                }
                else {
                    writable = false;
                    break;
                }
            }
        }

        if (writable && (rand1+wLength) <= array.length){
            for (var i = 0; i < wLength; i++){
                    array[rand1+i][rand2] = word.charAt(i);
            }
            return true;
        }
        else {
            return false;
        }
    }

    function addD(word, array){
        wLength = word.length;
        rand1 = Math.floor(Math.random()*(wordSearch.length - wLength));
        rand2 = Math.floor(Math.random()*(wordSearch.length - wLength));

        if (rand1 < 0){
            rand1 = 0;
        }
        if (rand2 < 0){
            rand2 = 0;
        }
        if ((rand1+wLength) <= array.length && (rand2+wLength) <= array[rand1].length){
            for(var n = 0; n < wLength; n++){
                if (typeof array[rand1+n][rand2+n] == 'undefined' || array[rand1+n][rand2+n] === null || array[rand1+n][rand2+n] == word.charAt(n)){
                    writable = true;
                }
                else {
                    writable = false;
                    break;
                }
            }
        }

        if (writable && (rand1+wLength) <= array.length && (rand2+wLength) <= array.length){
            for (var i = 0; i < wLength; i++){
                    array[rand1+i][rand2+i] = word.charAt(i);
            }
            return true;
        }
        else {
            return false;
        }
    }

    function selectDirection(word, array){
        var status = false;
        for (var i = 0; i < 1000; i++){
            status = false;
            if ((Math.floor(Math.random()*3) + 1) == 1) {
                status = addH(word, array);
            }
            else if ((Math.floor(Math.random()*3) + 1) == 2){
                status = addD(word, array);
            }
            else {
                status = addV(word, array);
            }
            if (status){
                break;
            }
        }
        if (!status){
            selectDirection(word, array);
            wordList.splice(wordList.indexOf(word), 1);
        }
    }

    for(var l = wordList.length-1; l >= 0; l--){
        wordList[l] = wordList[l].toUpperCase();
        selectDirection(wordList[l], wordSearch);
    }

    // OUTPUT
    function output(){
        var output = '';
        output += "<div class='PuzzleContent' id='"+id_PuzzleContent+"'>";
        
            output += "<div class='PuzzleContent__row'>";
                
                output += "<div class='PuzzleContent__cell'>";
                    
                    output += "<div class='tablePuzzle' id='"+id_tablePuzzle+"'>";
                        
                            for(var j = 0; j < wordSearch.length; j++){								
                                output += "<div class='tablePuzzle__row'>";
                                for(var k = 0; k < wordSearch[j].length; k++){
                                    if (typeof wordSearch[j][k] != 'undefined'){
                                        if (k != (wordSearch[j].length-1)){
                                            
                                            output += "<div  class='tablePuzzle__cell "+ id_tablePuzzleCell+j+"-"+k+"'><span id='"+j+"-"+k+"'>" + wordSearch[j][k] + "</span></div>";
                                        }
                                        else {
                                            output += "<div class='tablePuzzle__cell  "+id_tablePuzzleCell+j+"-"+k+"'><span id='"+j+"-"+k+"'>" + wordSearch[j][k] + "</span></div>";
                                            output += "</div>";
                                        }
                                    }
                                    else {
                                        if (k != (wordSearch[j].length-1)){
                                            wordSearch[j][k]=alphabet[Math.floor(Math.random()*alphabet.length)].toUpperCase();
                                            output += "<div class='tablePuzzle__cell "+id_tablePuzzleCell+j+"-"+k+"'><span id='"+j+"-"+k+"'>"+wordSearch[j][k]+"</span></div>";
                                        }
                                        else {
                                            wordSearch[j][k]=alphabet[Math.floor(Math.random()*alphabet.length)].toUpperCase();
                                            output += "<div class=' tablePuzzle__cell "+id_tablePuzzleCell+j+"-"+k+"'><span  id='"+j+"-"+k+"'>"+wordSearch[j][k]+"</span></div>";
                                            output += "</div>";
                                        }
                                    }
                                }
                            }
                        output += "</div>";
                    
                    output += "</div>";
                
                
                output += "<div class='PuzzleContent__cell'>";
                    
                    output += "<div class='tableAnswers' id='"+id_tableAnswers+"' class='anscss'>";
                        for (var i = 0; i < wordList.length; i++){
                            if (i % 2 === 0){
                                output += "<div class='answers'>";
                                output += wordList[i];
                                output += "</div>";
                            }
                            else {
                                output += "<div class='answers'>";
                                output += wordList[i];
                                output += "</div>";
                            }
                        }
                    output += "</div>"
                
                output += "</div>";
        
            output += "</div>";
                        
        output += "</div>";
        document.getElementById(PuzzleContent_id).innerHTML = output;
    };

    function main(){
        var colors =[ 
            '#FF6633', '#FFB399', '#854385', '#d3d386', '#029bc5', 
            '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
            '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A', 
            '#c76cb0', '#b1db17', '#FF1A66', '#E6331A', '#32c49f',
            '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC', 
            '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
            '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#05c06c', 
            '#4D8066', '#809980', '#FFB300', '#23c536', '#999933',
            '#FF3380', '#CCCC00', '#6fc25e', '#4D80CC', '#9900B3', 
            '#E64D66', '#4DB380', '#FF4D4D', '#6ea8a8', '#6666FF'
        ];
        var selectedArray = [];
        var compareString = "";
        var reversedString = "";
        var comparison = false;
        var isMouseDown = false;
        var prevID = 0;
        var touch = 0;
        // setInterval(function(){
        // // 		if (timerStarted){
        // 				tick++;
        // // 				timer = msToTime((tick*100));
        // 		}
        // }, 100);
        // if($("#wordArea")){
            // createWordList($("#wordArea").value);

        // }
        // // $("#timer").text(timer);
        $("#"+id_PuzzleContent+" #"+id_tableAnswers+" div span").each(function (i){
            if (!$(this).hasClass("correct")){
                allAnswered = false;
                return false;
            }
            else {
                allAnswered = true;
            }
        });
        if (allAnswered) {
            // timerStarted = false;
        }

        // $(document).mousedown(function () {
        //         return false;
        //     });
        // /*--------*/
        // $(document).mouseover (function (){
        //         return false;
        //     });
        /*--------*/
        $("#"+id_PuzzleContent+" #"+id_tablePuzzle+" div span").bind("mousedown touchstart", function () {
                if (!allAnswered){ // timerStarted = true;
                }
                isMouseDown = true;
                prevID = $(this).attr('id');
                if (!$(this).hasClass("highlighted")){
                    selectedArray.push($(this).text());
                    $(this).addClass("highlighted");
                }
                return false; // prevent text selection
        });
        /*--------*/
        $("#"+id_PuzzleContent+" #"+id_tablePuzzle+"")
                .bind("mousemove touchmove", function (e) {
                    var addedToArray;
                    e.preventDefault();
                    if ($(this).attr('id') != prevID){
                        addedToArray = false;
                    }
                    if (isMouseDown){
                        if (e.originalEvent.touches || e.originalEvent.changedTouches){
                            touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
                        }
                        else {
                            touch = e;
                        }
                        for (var q = 0; q < dimensions; q++){
                            for (var a = 0; a < dimensions; a++){
                                var offset = $("#"+q+"-"+a).offset();
                                var x = touch.pageX - offset.left;
                                var y = touch.pageY - offset.top;

                                if (x < $("#"+q+"-"+a).outerWidth()-4 && x > 4 && y < $("#"+q+"-"+a).outerHeight()-4 && y > 2 && !addedToArray && !$("#"+q+"-"+a).hasClass("highlighted")){
                                        //trace('x: ' + x + ' y: ' + y);
                                        addedToArray = true;
                                        selectedArray.push($("#"+q+"-"+a).text());
                                        prevID = $("#"+q+"-"+a).attr('id');
                                        $("#"+q+"-"+a).addClass("highlighted");
                                        break;
                                }
                            }
                        }
                    }
                        })
                .bind("selectstart", function (e) {
                            return false; // prevent text selection in IE
                        });
        /*--------*/
        $(document).bind("mouseup touchend", function () {
            prevID = 0;
            isMouseDown = false;
            comparison = false;
            compareString = "";
            reversedString = "";

            for(var i = 0; i < selectedArray.length; i++){
                compareString = compareString.concat(selectedArray[i]);
            }
            for(var j = 0; j < wordList.length; j++){				
                var color_random = colors[Math.floor(Math.random()* colors.length)];
                
                if (compareString == wordList[j]){
                    answeredList.push(wordList[j]);
                    comparison = true;
                    $("#"+id_PuzzleContent+" .tablePuzzle__row div span.highlighted").each(function (i){
                        if (!$(this).hasClass("correct")){
                        $(this).removeClass("highlighted");
                        $(this).addClass("correct");
                        $(this).css("background-color", color_random);
                    }
                    });
                    $("#"+id_PuzzleContent+" #"+id_tableAnswers+" .answers").each(function (i){
                        if ($(this).text() == compareString && !$(this).hasClass("correct")){
                            $(this).removeClass("answers");
                            $(this).addClass("correct");
                            $(this).css("background-color", color_random);
                            return false;
                        }
                    });
                    break;
                }
                reversedString = compareString.split('').reverse().join('');
                
                if (reversedString == wordList[j]){
                    answeredList.push(wordList[j]);
                    comparison = true;
                    $("#"+id_PuzzleContent+" #"+id_PuzzleContent+" .tablePuzzle__row div span.highlighted").each(function (i){
                        if (!$(this).hasClass("correct")){
                        $(this).removeClass("highlighted");
                        $(this).addClass("correct");
                        $(this).css("background-color", color_random);
                    }
                    });
                    $("#"+id_PuzzleContent+" #"+id_PuzzleContent+" #"+id_tableAnswers+" .answers").each(function (i){
                        if ($(this).text() == reversedString && !$(this).hasClass("correct")){
                            $(this).removeClass("answers");
                            $(this).addClass("correct");
                            $(this).css("background-color", color_random);
                            return false;
                        }
                    });
                    break;
                }
            }
            $("#"+id_PuzzleContent+" .tablePuzzle__row div span.highlighted").each(function (i){
                $(this).removeClass("highlighted");
            });
            for(var k = selectedArray.length-1; k >= 0; k--){
                    selectedArray.splice(k,1);
                }

        });
    };

    output();
    main();
    
    $(".tablePuzzle__cell").css({
            "width":100/dimensions + "%",
            "paddingTop":100/dimensions + "%"
    });
}